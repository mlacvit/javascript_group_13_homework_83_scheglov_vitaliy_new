const mongoose = require('mongoose');
const config = require('./config');
const Album = require('./models/Album');
const Artist = require('./models/Artist');

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [dudebox, bridge] = await Album.create({
      title: 'The Bridge',
      year: '2021',
      image: 'W3Jl7Jj_Lbd5sVTQtFNBR.png',
    },
    {
      title: 'Dudebox',
      year: '1995',
      image: 'Dudebox.jpg',
    })

  await Artist.create({
      title: 'Billy Talent',
    description: 'канадская рок-группа. Была сформирована под названием Pezz в Онтарио в 1993 году. Состав группы: Бенджамин Ковалевич, Иэн Ди’Сэй, Джонатан Гэллант и Аарон Соловонюк',
    image: 'PXEwATM3fsmThZQyzquGS.jpeg',
    album: dudebox
  },
    {
      title: 'Sting',
      description: 'британский музыкант-мультиинструменталист, певец и автор песен, актёр, общественный деятель и филантроп. Вокалист группы The Police в 1976—1984 годах. С 1984 года выступает сольно',
      image: 'Xebp187sK28JCAB198NBL.jpg',
      album: bridge
    },
    )

  await mongoose.connection.close();
};


run().catch(e => console.error(e));