const express = require('express');
const app = express();
const mongoose = require("mongoose");
const config = require('./config');
const artist = require('./app/artists');
const album = require('./app/albums');
const users = require('./app/users');
const track = require('./app/track');

const cors = require('cors');

const port = 8000;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());
app.use(express.static('public'));
app.use('/artist', artist);
app.use('/album', album);
app.use('/users', users);
app.use('/track', track);

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);
}

app.listen(port, () => {
  console.log('We are live on ' + port);
});

 process.on('exit', async () => {
 await mongoose.disconnect();
})

run().catch(e => console.error(e));