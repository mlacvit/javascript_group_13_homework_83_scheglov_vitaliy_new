const express = require('express');
const config = require('../config');
const path = require('path');
const Artist = require("../models/Artist");

const { nanoid } = require('nanoid');
const multer = require('multer');

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {


    const artists = await Artist.find();

    return res.send(artists);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const artist = await Artist.findById(req.params.id);

    if (!artist) {
      return res.status(404).send({message: 'Not found'});
    }

    return res.send(artist);
  } catch (e) {
    next(e);
  }
});

router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.title) {
      return res.status(400).send({message: 'title are required'});
    }

    const musData = {
      title: req.body.title,
      description: req.body.description,
      image: null,
    };

    if (req.file) {
      musData.image = req.file.filename;
    }

    const musBase = new Artist(musData);

    await musBase.save();

    return res.send({message: 'Created new artist', id: musBase._id});
  } catch (e) {
    next(e);
  }
});

module.exports = router;