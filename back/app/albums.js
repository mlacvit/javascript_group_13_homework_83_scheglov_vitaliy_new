const express = require('express');
const config = require('../config');
const path = require('path');
const Album = require("../models/Album");

const { nanoid } = require('nanoid');
const multer = require('multer');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
    try {
        const albums = await Album.find().populate('artist');
        return res.send(albums);
    } catch (e) {
        next(e);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        const albums = await Album.findById(req.params.id);

        if (!albums) {
            return res.status(404).send({message: 'Not found'});
        }

        return res.send(albums);
    } catch (e) {
        next(e);
    }
});

router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        if (!req.body.title || !req.body.artist) {
            return res.status(400).send({message: 'artist & title are required'});
        }

        const albums = {
            title: req.body.title,
            artist: req.body.artist,
            year: req.body.year,
            image: null,
        };

        if (req.file) {
            albums.image = req.file.filename;
        }

        const MusAlbums = new Album(albums);

        await MusAlbums.save();

        return res.send({message: 'Created new album', id: MusAlbums._id});
    } catch (e) {
        next(e);
    }
});

module.exports = router;