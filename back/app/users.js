const express = require('express');
const config = require('../config');
const path = require('path');
const User = require("../models/User");
const { nanoid } = require('nanoid');
const multer = require('multer');
const mongoose = require("mongoose");
const auth = require("../middleware/auth");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    const user =  await User.find();

    return res.send(user);
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError){
      return res.status(400).send(e);
    }
    next(e);
  }
});


router.post('/', upload.single('image'), async (req, res, next) => {
  try {

    const user = new User(req.body);

    if (req.file) {
      user.image = req.file.filename;
    }

    user.generateToken();

    await user.save();

    return res.send({message: 'Created new user', id: user._id});
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError){
      return res.status(400).send(e);
    }
    next(e);
  }
});

router.post('/sessions', async (req, res, next) =>{
  try {

    const user = await User.findOne({username: req.body.username});
  if (!user){
    return res.status(400).send({error: 'username not found'});
  }

  const isMatch = await user.checkPassword(req.body.password);

  if (!isMatch){
    return res.status(400).send({error: 'Password is wrong'});
  }

  user.generateToken();
  await user.save();

  return res.send({message: 'Username & password correct!', user});

  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError){
      return res.status(400).send(e);
    }
    next(e);
  }
});

router.post('/auth', auth, async (req, res, next) =>{
  try {
    return res.send({message: 'Welcome!' + req.user.username})

  } catch (e) {
    next(e);
  }
})

module.exports = router;