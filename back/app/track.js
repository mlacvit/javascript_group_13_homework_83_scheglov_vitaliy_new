const express = require('express');
const mongoose = require("mongoose");
const Track = require("../models/Track");
const router = express.Router();



router.get('/', async (req, res, next) => {
  try {
    const track =  await Track.find().populate('album');

    return res.send(track);
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError){
      return res.status(400).send(e);
    }
    next(e);
  }
});

router.post('/', async (req, res, next) =>{
  try {

    const trackBase = new Track({
      title: req.body.title,
      album: req.body.album,
      timing: req.body.timing,
    });

    await trackBase.save();

    return res.send({message: 'Created new track', id: trackBase._id});
  } catch (e) {
    next(e);
  }
})

module.exports = router;