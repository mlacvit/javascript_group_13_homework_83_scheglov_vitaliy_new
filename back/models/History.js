const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const HistorySchema = new Schema({
  track: String,
  user: String,
  date: String,
});

const History = mongoose.model('History', HistorySchema);

module.exports = History;