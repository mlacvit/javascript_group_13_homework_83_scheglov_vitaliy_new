const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const albumSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    artist: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'artist',
    },
    year: String,
    image: String
});

const Album = mongoose.model('Album', albumSchema);

module.exports = Album;