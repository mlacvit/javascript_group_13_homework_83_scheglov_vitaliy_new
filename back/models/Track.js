const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TrackSchema = new Schema({

  title: String,
  album: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Album'
  },
  timing: String,

});

const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;