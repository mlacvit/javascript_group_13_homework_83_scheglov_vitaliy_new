const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ArtistSchema = new Schema({

  title: {
    type: String,
    required: true
  },
  description: String,
  image: String,

});

const Artist = mongoose.model('artist', ArtistSchema);

module.exports = Artist;