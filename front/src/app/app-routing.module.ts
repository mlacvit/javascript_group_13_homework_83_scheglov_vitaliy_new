import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {EditComponent} from "./edit/edit.component";
import {MusicComponent} from "./music/music.component";
import {NewalbumComponent} from "./newalbum/newalbum.component";
import { RegisterComponent } from './register/register.component';


const routes: Routes = [
  {path: 'newalbum', component: NewalbumComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'edit', component: EditComponent},
  {path: '', component: HomeComponent},
  {path: ':id', component: MusicComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
