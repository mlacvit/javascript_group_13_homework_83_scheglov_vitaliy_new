import { createAction, props } from '@ngrx/store';
import { User, UserData, UserError } from '../models/user.model';

export const regUserReq = createAction(
  '[Users] users req' ,
  props<{userData: UserData}>()
  );
export const regUserSus = createAction(
  '[Users] users sus',
  props<{user: User}>()
  );
export const regUserFai = createAction(
  '[Users] users fai' ,
  props<{error: null | UserError}>()
  );
