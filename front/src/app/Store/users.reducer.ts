import { UserState } from './types';
import { createReducer, on } from '@ngrx/store';
import { regUserFai, regUserReq, regUserSus } from './users.actions';

const initialState: UserState = {
  user: null,
  regload: false,
  regError: null,
};

export const userReducer = createReducer(
  initialState,
  on(regUserReq, state => ({...state, regload: true, regError: null})),
  on(regUserSus, (state, {user}) => ({
    ...state,
    regload: false,
    user
  })),
  on(regUserFai, (state, {error}) => ({
    ...state,
    regload: false,
    regError: error
  })),
)
