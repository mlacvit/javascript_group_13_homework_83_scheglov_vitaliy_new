import { MusicModel } from '../models/music.model';
import { User, UserError } from '../models/user.model';

export type MusicState = {
  music: MusicModel[],
  loading: boolean,
  error: null | string,
  createLoading: boolean,
  createError: null | string,
};

export type AppState = {
  music: MusicState,
  users: UserState,
}

export type UserState = {
  user: null | User,
  regload: boolean,
  regError: null | UserError
}

