import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { regUserFai, regUserReq, regUserSus } from './users.actions';
import { catchError, mergeMap, of, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';


@Injectable()

export class UserEffects {
  constructor(
    private actions: Actions,
    private service: UserService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  registerUser = createEffect(() => this.actions.pipe(
    ofType(regUserReq),
    mergeMap(({userData}) => this.service.registerUser(userData).pipe(
      map(user => regUserSus({user})),
      tap(() => {
        this.snackBar.open('register successful', 'ok', {duration: 5000});
      },
      void this.router.navigate(['/'])
      ),
      catchError(regErr => {
        let registerError = null;
        if (regErr instanceof HttpErrorResponse && regErr.status === 400){
          registerError = regErr.error;
        }else {
          this.snackBar.open('server error', 'ok', {duration: 5000})
        }
        return of(regUserFai({error: registerError}))
      })
    ))
  ))
}
