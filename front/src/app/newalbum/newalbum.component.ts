import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {MusicService} from "../services/music.service";
import {Router} from "@angular/router";
import {AlbumData} from "../models/album.model";
import {MusicModel} from "../models/music.model";
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { fetchMusicReq } from '../Store/music.actions';
import { AppState } from '../Store/types';

@Component({
  selector: 'app-newalbum',
  templateUrl: './newalbum.component.html',
  styleUrls: ['./newalbum.component.sass']
})
export class NewalbumComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  music: Observable<MusicModel[] | null>;
  changeSub!: Subscription;
  constructor(private store: Store<AppState>) {
    this.music = store.select(state => state.music.music);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchMusicReq());
  }

  onSubAlbum() {
    const value: AlbumData = this.form.value;
    // // this.service.createAlbums(value).subscribe(()=>{
    //
    // });
  }

}
