import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../Store/types';
import { Observable, Subscription } from 'rxjs';
import { UserError } from '../models/user.model';
import { regUserReq } from '../Store/users.actions';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements AfterViewInit, OnDestroy {
  @ViewChild('f') form!: NgForm;
  error: Observable<null | UserError>;
  loading: Observable<boolean>;
  errorSub!: Subscription;
  constructor(private store: Store<AppState>) {
    this.error = store.select(state => state.users.regError);
    this.loading = store.select(state => state.users.regload);
  }


  onSubRegister() {
    this.store.dispatch(regUserReq({userData: this.form.value}))
  }

  getPass(e: any) {
    console.log(e.target.value)
  }

  ngAfterViewInit(): void {
    this.errorSub = this.error.subscribe(error => {
if (error){
  const msg = error.errors.email.message;
  this.form.form.get('email')?.setErrors({serverError: msg})
}else {
  this.form.form.get('email')?.setErrors({})
}
    })
  }

  ngOnDestroy(): void {
    this.errorSub.unsubscribe();
  }
}
