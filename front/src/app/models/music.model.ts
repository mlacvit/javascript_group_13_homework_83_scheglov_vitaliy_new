export class MusicModel {
  constructor(
    public _id: string,
    public title: string,
    public description: string,
    public image: string,
  ) {}
}


export interface MusicData {
  [key: string]: any;
  title: string;
  description: string;
  image: File | null;
}

export interface ApiMusicData {
  _id: string,
  title: string,
  description: string,
  image: string,
}
