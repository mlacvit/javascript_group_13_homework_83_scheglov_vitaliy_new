import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, UserData } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  registerUser(userData: UserData){
    return this.http.post<User>('http://localhost:8000/users', userData);
  }

}
