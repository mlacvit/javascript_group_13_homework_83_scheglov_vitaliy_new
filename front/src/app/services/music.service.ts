import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { MusicData, MusicModel } from '../models/music.model';
import { AlbumData, AlbumModel } from '../models/album.model';

import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class MusicService {

  constructor(private http: HttpClient) { }

  createMusic(value: MusicData) {
    const formData = new FormData();
    Object.keys(value).forEach(key => {
      if (value[key] !== null) {
        formData.append(key, value[key]);
      }
    });
    return this.http.post('http://localhost:8000/artist', formData);
  };

  createAlbums(AlbumData: AlbumData) {
    const formData = new FormData();
    Object.keys(AlbumData).forEach(key => {
      if (AlbumData[key] !== null) {
        formData.append(key, AlbumData[key]);
      }
    });
    return this.http.post('http://localhost:8000/album', formData);
  };


  getMusic() {
    return this.http.get<MusicModel[]>('http://localhost:8000/artist');
  }

  geMusicOne(id: string) {
    return this.http.get<MusicModel>(`http://localhost:8000/artist/${id}`);
  }

  getAlbums(){
    return this.http.get<AlbumModel[]>(`http://localhost:8000/album`);
  }

}
